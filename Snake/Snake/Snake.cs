﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Snake
{

    class Snake
    {

        #region Vars

        private static LinkedList<Snake> allSnakes = new LinkedList<Snake>();
        private static int SerialnumberCounter;
        private int Serialnumber;

        private const int RECT_WIDTH = 5;
        private const int RECT_HEIGHT = 5;

        public static Vector2 UP = new Vector2(0, -RECT_HEIGHT);
        public static Vector2 DOWN = new Vector2(0, RECT_HEIGHT);
        public static Vector2 LEFT = new Vector2(-RECT_WIDTH, 0);
        public static Vector2 RIGHT = new Vector2(RECT_WIDTH, 0);

        private Vector2 _position;
        private Vector2 _movement = new Vector2(0, 0);

        private Rectangle _textureRect;

        private int feedEaten = 0;

        #endregion


        #region Initialisation

        public Snake()
            : this(new Vector2(0, 0))
        {

        }

        public Snake(Vector2 pos)
        {
            Position = pos;

            allSnakes.AddLast(this);
            this.Serialnumber = SerialnumberCounter++;
        }


        /// <summary>
        /// Methos clears the complete chain
        /// </summary>
        public static void Clear()
        {
            Snake.allSnakes.Clear();
        }

        /// <summary>
        /// adds a new chain element in current position
        /// </summary>
        public void increment()
        {
            new Snake(this.Position);
        }

        #endregion

        #region movement

        /// <summary>
        /// moves the first chain element
        /// </summary>
        /// <param name="view"></param>
        public void move(Viewport view)
        {

            moveNext();
            this.Position += _movement;
            if (Position.X > view.Width - RECT_WIDTH)
                Position = new Vector2(0, Position.Y);
            else if (Position.X < 0)
                Position = new Vector2(view.Width - RECT_WIDTH, Position.Y);
            else if (Position.Y > view.Height - RECT_HEIGHT)
                Position = new Vector2(Position.X, 0);
            else if (Position.Y < 0)
                Position = new Vector2(Position.X, view.Height - RECT_HEIGHT);
        }

        /// <summary>
        /// moves the next chain element. Procedure is initial called in procedure 'move'
        /// </summary>
        private void moveNext()
        {
            if (Next.Next != null) Next.moveNext();
            Next.Position = this.Position;
        }

        #endregion

        #region Properties

        public int FeedEaten
        {
            get { return this.feedEaten; }
            set
            {
                if (value < 0) return;
                this.feedEaten = value;
            }
        }
        /// <summary>
        /// Gets the next chain element of snake
        /// </summary>
        public Snake Next
        {
            get
            {
                return (allSnakes.Find(this).Next == null) ? null : allSnakes.Find(this).Next.Value;
            }
            set { allSnakes.Find(this).Next.Value = value; }
        }

        /// <summary>
        /// Gets the previous chain element of snake
        /// </summary>
        public Snake Prev
        {
            get
            {
                return (allSnakes.Find(this).Previous == null) ? null : allSnakes.Find(this).Previous.Value;
            }
            set { allSnakes.Find(this).Previous.Value = value; }
        }

        /// <summary>
        /// Gets the last chain element of snake
        /// </summary>
        public Snake Last
        {
            get
            {
                return (allSnakes.Last == null) ? null : allSnakes.Last.Value;
            }
        }

        public Vector2 Position
        {
            get { return _position; }
            set
            {
                this._position = value;
                _textureRect = new Rectangle((int)_position.X, (int)_position.Y, RECT_WIDTH, RECT_HEIGHT);
            }
        }

        /// <summary>
        /// move direction will be changed
        /// </summary>
        public Vector2 movementDirection
        {
            set
            {
                if (Next == null || this.Position + value != Next.Position)
                    _movement = value;
            }
            get { return _movement; }
        }

        #endregion

        #region graphical

        /// <summary>
        /// draws the single chain element to the given spriteBatch
        /// </summary>
        /// <param name="g"></param>
        /// <param name="s"></param>
        private void draw(GraphicsDevice g, SpriteBatch s)
        {
            Texture2D dummyTexture = new Texture2D(g, 1, 1);
            dummyTexture.SetData(new Color[] { Color.White });
            s.Begin();
            s.Draw(dummyTexture, _textureRect, Color.Black);
            s.End();
        }

        /// <summary>
        /// draws all chain elements to the given spriteBatch
        /// </summary>
        /// <param name="g"></param>
        /// <param name="sprite"></param>
        public void drawAll(GraphicsDevice g, SpriteBatch sprite)
        {
            foreach (Snake s in allSnakes.ToArray<Snake>())
            {
                s.draw(g, sprite);
            }
        }

        #endregion



    }
}
