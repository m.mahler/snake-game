using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Kyle0654.Components;

namespace Snake
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private Snake snake = new Snake();
        private Feed feed;
        private double movementTime = 0;

        QuadTree<Texture2D> quadTree;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 200;
            graphics.PreferredBackBufferWidth = 200;
            Content.RootDirectory = "Content";

            quadTree = new QuadTree<Texture2D>(new FRect(0, 0, graphics.PreferredBackBufferHeight, graphics.PreferredBackBufferWidth), 5);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Reset();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            KeyboardState kState = Keyboard.GetState();
            if (kState.IsKeyDown(Keys.Up)) this.snake.movementDirection = Snake.UP;
            if (kState.IsKeyDown(Keys.Down)) this.snake.movementDirection = Snake.DOWN;
            if (kState.IsKeyDown(Keys.Right)) this.snake.movementDirection = Snake.RIGHT;
            if (kState.IsKeyDown(Keys.Left)) this.snake.movementDirection = Snake.LEFT;

            if (gameTime.TotalGameTime.TotalSeconds >= movementTime / 10)
            {

                // check collition with own tail
                Snake snaketailElement = snake.Next;
                while ((snaketailElement = snaketailElement.Next) != null)
                {
                    if (snaketailElement.Position == snake.Position)
                    {
                        Reset();
                        break;
                    }
                }

                //check collision with feed
                if (snake.Position == feed.Position)
                {
                    feed = new Feed(GraphicsDevice.Viewport);
                    snake.FeedEaten++;
                    snake.increment();
                }

                // move snake
                snake.move(GraphicsDevice.Viewport);
                movementTime += 1;

            }
            

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);
            this.feed.draw(GraphicsDevice, spriteBatch);
            this.snake.drawAll(GraphicsDevice, spriteBatch);
            
            base.Draw(gameTime);
        }

        public void Reset()
        {
            Snake.Clear();
            this.snake = new Snake(new Vector2(graphics.GraphicsDevice.Viewport.Width / 2, graphics.GraphicsDevice.Viewport.Height / 2));
            this.snake.movementDirection = Snake.RIGHT;
            for (int i = 0; i < 4; i++)
            {
                this.snake.increment();
            }
            this.feed = new Feed(GraphicsDevice.Viewport);
        }
    }
}
