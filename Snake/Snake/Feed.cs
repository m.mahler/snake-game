﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Snake
{
    class Feed
    {
        private const int RECT_WIDTH = 5;
        private const int RECT_HEIGHT = 5;

        private Vector2 _position;

        private Rectangle _textureRect;

        #region Initalization

        public Feed()
        {
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="view">The viewport within feed will appear</param>
        public Feed(Viewport view)
        {
            // determine random position within the viewport
            Random rnd = new Random();
            int rndX = -1;
            while (rndX % RECT_WIDTH != 0)
                rndX = rnd.Next(0, view.Width);
            int rndY = -1;
            while (rndY % RECT_HEIGHT != 0)
                rndY = rnd.Next(0, view.Height);
            Position = new Vector2(rndX, rndY);

        }

        #endregion


        #region Properties

        public Vector2 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                _textureRect = new Rectangle((int)_position.X, (int)_position.Y, RECT_WIDTH, RECT_HEIGHT);
            }
        }

        #endregion

        #region graphical

        /// <summary>
        /// draws the single feed box to the given spriteBatch
        /// </summary>
        /// <param name="g"></param>
        /// <param name="s"></param>
        public void draw(GraphicsDevice g, SpriteBatch s)
        {
            Texture2D dummyTexture = new Texture2D(g, 1, 1);
            dummyTexture.SetData(new Color[] { Color.White });
            s.Begin();
            s.Draw(dummyTexture, _textureRect, Color.Blue);
            s.End();
        }

        #endregion

    }
}
